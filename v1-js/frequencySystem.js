function getFrequency(text) {
  const frequency = {};
  for (let i of text.split(" ")) {
    frequency[i] ? frequency[i]++ : (frequency[i] = 1);
  }
  return frequency;
}

function getMostFrequentWords(frequencyObject, required = 0) {
  frequencyObject = getFrequency(frequencyObject);
  const highestValues = [
    Object.keys(frequencyObject).sort(
      (elementA, elementB) =>
        frequencyObject[elementB] - frequencyObject[elementA]
    ),
    Object.values(frequencyObject).sort(
      (elementA, elementB) => elementB - elementA
    ),
  ];
  frequencyObject = {};

  for (let i in required
    ? highestValues[0].slice(0, required)
    : highestValues[0]) {
    frequencyObject[highestValues[0][i]] = highestValues[1][i];
  }
  return frequencyObject;
}

module.exports.options = { getMostFrequentWords };
