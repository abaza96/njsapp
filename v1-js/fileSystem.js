const fileSystem = require("fs");

function getFileContent(filePath, encode = "utf8") {
  return new Promise((resolve, reject) =>
    fileSystem.readFile(filePath, encode, (err, data) =>
      err ? reject(new Error("Error was Found")) : resolve(data)
    )
  );
}

module.exports.options = { getFileContent };
