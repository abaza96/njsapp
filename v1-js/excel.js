const excel = require("exceljs");
const workbook = new excel.Workbook();
const worksheet = workbook.addWorksheet("Most Frequent Words");

function createSheet(data, sheetName = "sheet") {
  worksheet.columns = [
    { header: "Word", key: "frequentWord" },
    { header: "Count", key: "wordCount" },
  ];
  for (let i in data) {
    worksheet.addRow({ frequentWord: i, wordCount: data[i] });
  }
  workbook.xlsx
    .writeFile(`./dist/${sheetName}.xlsx`)
    .then(console.log("Creating Sheet: success"))
    .catch(console.error("Error found"));
}

module.exports.options = { createSheet };
