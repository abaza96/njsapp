const fileSystem = require("./fileSystem");
const frequency = require("./frequencySystem");
const server = require("./appServer");
const regEx = /[^A-Za-z ]/g;
let result = "";

function extractData(path, run = false, sheetName = "Most Frequent Words") {
  return fileSystem.options
    .getFileContent(path)
    .then((data) => {
      result += data.replace(regEx, "");
      return run
        ? server.options.runServer(
            frequency.options.getMostFrequentWords(result, 10),
            sheetName
          )
        : result;
    })
    .catch((err) => console.log(err));
}

module.exports.options = { extractData };
