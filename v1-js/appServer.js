const http = require("http");
const excel = require("./excel");

function runServer(data, sheetName) {
  const server = http.createServer((req, res) => {
    if (req.url === "/") {
      excel.options.createSheet(data, sheetName);
      res.write(JSON.stringify(data));
      return res.end();
    }
  });
  server.listen(5000, () => console.log("Server Started on Port: 5000"));
}

module.exports.options = { runServer };
